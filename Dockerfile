FROM openjdk:17-jdk-alpine

COPY target/demo_tww-1.0-SNAPSHOT.jar app.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/app.jar"]
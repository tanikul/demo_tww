package com.tww.constant;

public enum Status {

    PENDING,
    APPROVE,
    REJECT,
    CANCEL
}

package com.tww.dto;

import com.tww.constant.Status;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CustomerDto {

    private String id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotNull
    private Integer age;

    @NotBlank
    private String email;

    @NotBlank
    private String houseNo;

    private String swine;

    private String address;

    @NotBlank
    private String district;

    private String subDistrict;

    @NotBlank
    private String province;

    private String postCode;

    private String photo;

    private Status status;
}

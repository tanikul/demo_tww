package com.tww.dto;

import com.tww.constant.Status;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CustomerStatusRequest {

    @NotNull
    private Status status;
}

package com.tww.controller;

import com.tww.dto.CustomerDto;
import com.tww.dto.CustomerStatusRequest;
import com.tww.service.CustomerService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/customers")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@Valid @RequestPart("data") CustomerDto customerDto,
                                    @NotNull @RequestPart("photo") MultipartFile photo) throws Exception {
        log.info("[Request] CustomerController.create: {}", customerDto.toString());
        CustomerDto customer = customerService.create(customerDto, photo);
        log.info("[Response] CustomerController.create: {}", customerDto);
        return ResponseEntity.ok(customer);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody @Valid CustomerDto customerDto,
                                    @RequestPart("photo") MultipartFile photo) throws Exception {
        log.info("[Request] CustomerController.update data: {}, id: {}", customerDto.toString(), id);
        CustomerDto customer = customerService.update(id, customerDto, photo);
        log.info("[Request] CustomerController.update: {}", customerDto);
        return ResponseEntity.ok(customer);
    }

    /* Cancel register by customer */
    @PatchMapping("/cancel/{id}")
    public ResponseEntity<?> cancel(@PathVariable("id") String id) {
        log.info("[Request] CustomerController.cancel id: {}", id);
        customerService.cancel(id);
        return ResponseEntity.ok().build();
    }

    /* Assume update status Approve or Reject by Admin*/
    @PatchMapping("/status/{id}")
    public ResponseEntity<?> updateStatus(@PathVariable("id") String id, @Valid @RequestBody CustomerStatusRequest customerUpdateStatusRequest) {
        log.info("[Request] CustomerController.updateStatus id: {}, body: {}", id, customerUpdateStatusRequest);
        customerService.updateStatus(id, customerUpdateStatusRequest);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        log.info("[Request] CustomerController.delete id: {}", id);
        customerService.delete(id);
        return ResponseEntity.ok().build();
    }
}

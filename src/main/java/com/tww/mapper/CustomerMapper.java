package com.tww.mapper;

import com.tww.dto.CustomerDto;
import com.tww.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    Customer toModel(CustomerDto dto);

    CustomerDto toDto(Customer dto);
}

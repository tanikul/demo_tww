package com.tww.repository;

import com.tww.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends ElasticsearchRepository<Customer, String> {

    Page<Customer> findAll();
    List<Customer> findByFirstName(String firstName);
}
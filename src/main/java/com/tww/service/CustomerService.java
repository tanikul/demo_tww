package com.tww.service;

import com.tww.dto.CustomerDto;
import com.tww.dto.CustomerStatusRequest;
import org.springframework.web.multipart.MultipartFile;

public interface CustomerService {

    CustomerDto create(CustomerDto customerDto, MultipartFile photo) throws Exception;
    CustomerDto update(String id, CustomerDto customerDto, MultipartFile photo) throws Exception;
    CustomerDto cancel(String id);
    CustomerDto updateStatus(String id, CustomerStatusRequest status);
    void delete(String id);
}

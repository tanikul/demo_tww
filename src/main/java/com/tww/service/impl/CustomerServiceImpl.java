package com.tww.service.impl;

import com.tww.constant.Status;
import com.tww.dto.CustomerDto;
import com.tww.dto.CustomerStatusRequest;
import com.tww.exception.NotFoundException;
import com.tww.mapper.CustomerMapper;
import com.tww.model.Customer;
import com.tww.repository.CustomerRepository;
import com.tww.service.CustomerService;
import com.tww.service.S3Service;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;
    private final S3Service s3Service;

    @Override
    public CustomerDto create(CustomerDto customerDto, MultipartFile photo) throws Exception {
        String fileName = s3Service.upload(photo);
        customerDto.setPhoto(fileName);
        customerDto.setStatus(Status.PENDING);
        Customer customer = customerRepository.save(customerMapper.toModel(customerDto));
        return customerMapper.toDto(customer);
    }

    @Override
    public CustomerDto update(String id, CustomerDto customerDto, MultipartFile photo) throws Exception {
        findCustomerById(id);
        customerDto.setId(id);
        if (!photo.isEmpty()) {
            String fileName = s3Service.upload(photo);
            customerDto.setPhoto(fileName);
        }
        Customer customer = customerRepository.save(customerMapper.toModel(customerDto));
        return customerMapper.toDto(customer);
    }

    @Override
    public CustomerDto cancel(String id) {
        return updateStatus(id, Status.CANCEL);
    }

    @Override
    public CustomerDto updateStatus(String id, CustomerStatusRequest status) {
        return updateStatus(id, status.getStatus());
    }

    @Override
    public void delete(String id) {
        Customer customer = findCustomerById(id);
        s3Service.deleteFile(customer.getPhoto());
        customerRepository.deleteById(id);
    }

    private CustomerDto updateStatus(String id, Status status) {
        Customer customer = findCustomerById(id);
        customer.setStatus(status);
        return customerMapper.toDto(customerRepository.save(customer));
    }

    private Customer findCustomerById(String id) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isEmpty()) {
            throw new NotFoundException("Customer not found");
        }
        return customerOptional.get();
    }
}
